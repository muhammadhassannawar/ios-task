//
//  TestSearchModule.swift
//  iOS TaskTests
//
//  Created by Mohamed Hassan Nawar on 26/06/2021.
//

import XCTest
import Foundation
import Mockit

class TestSearchModule: XCTestCase {
    var coreNetwork: MockCoreNetwork<SearchResult>!
    var presenter: MockSearchPresenter!
    var interator: SearchInteractor!
    var view: MockSearchView!
    var documents: [Document] = []
    var expextedCount: Int = 0
    let requiredInputErrorMessage = "Please provide a valid value for search term " +
        "and select one ore more category"
    
    override func setUpWithError() throws {
        expextedCount = 0
        testMangingDocumentsLogic()
        view = MockSearchView.init(testCase: self)
        coreNetwork = MockCoreNetwork<SearchResult>()
        interator = SearchInteractor(coreNetwork: coreNetwork)
        presenter = MockSearchPresenter(view: view, interactor: interator, testCase: self)
    }
    
    func testMangingDocumentsLogic() {
        let result = SearchResult(start: documents.count, docs: documents)
        assert(result.docs.allSatisfy({ resultSection in
            documents.contains(where: { expected in
                resultSection.title == expected.title
            })
        }))
    }
    
    func testDidLoadDocuments() throws {
        self.presenter.didLoadDocuments(models: documents, page: 1)
        view.showSearchResults(with: documents)
        wait(for: 3)
    }
    
    func testdDidLoadSearchResults() throws {
        let result = SearchResult.init(start: expextedCount, docs: documents)
        coreNetwork?.result = result
        coreNetwork?.error = nil
        view?.verify(verificationMode: AtMostOnce())
            .showLoadingView()
        interator?.loadDocuments(searchType: .querySearch, page: 1, searchText: "we") { [weak self]
            result, error in
            if error != nil {
            }else{
                guard let documents = result?.docs else { return }
                self?.documents = documents
                
            }
        }
        view?.verify(verificationMode: AtMostOnce())
            .hideLoadingView()
        wait(for: 3)
    }
    
    func testConfigurator() throws {
        guard let viewController = SearchViewController.loadFromNib() else { assert(false) }
        SearchConfigurator().configure(viewController: viewController)
        assert(viewController.presenter != nil)
    }
    
    func testEmptyDocuments() throws {
        view?.verify(verificationMode: AtMostOnce())
            .showLoadingView()
        interator?.loadDocuments(searchType: .querySearch, page: 1, searchText: "") { [weak self]
            result, error in
            if error == nil {
                self?.view?.verify(verificationMode: AtMostOnce()).showMessage(text: error ?? "")
            }else{
                guard let documents = result?.docs else { return }
                self?.documents = documents
            }
        }
        view?.verify(verificationMode: AtMostOnce())
            .hideLoadingView()
    }
    
    func testFailedToLoadSearchResults() throws {
        coreNetwork?.result = nil
        coreNetwork?.error = NSError(domain: "", code: 400,
                                     userInfo: [ NSLocalizedDescriptionKey: "Failed To Load Data"])
        presenter?.searchBySpecificText(text: "")
        wait(for: 3)
        view?.verify(verificationMode: Once()).hideLoadingView()
        view?.verify(verificationMode: Once()).showMessage(text: "Failed To Load Data")
        presenter?.verify(verificationMode: Once()).didChangeSearchType(searchType: .querySearch)
    }
    
    func testViewController() throws {
        guard let viewController = SearchViewController.loadFromNib() else { assert(false) }
        viewController.loadViewIfNeeded()
        let parent = UINavigationController.init(rootViewController: viewController)
        viewController.showLoadingView()
        viewController.hideLoadingView()
        viewController.didSelectDocumentTitle(title: "valid")
        viewController.viewWillAppear(false)
        viewController.viewWillDisappear(false)
        assert(!parent.isNavigationBarHidden)
        viewController.presenter = presenter
        viewController.updateSearchBarText(text: "as")
        wait(for: 3)
        presenter.verify(verificationMode: Once())
            .didLoadDocuments(models: documents, page: 1)
    }
}

class MockSearchView: SearchView, Mock {
    func showMessage(text: String) {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: text)
    }
    
    func showSearchResults(with results: [Document]) {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: results)
    }
    
    func showLoadingView() {
        callHandler.accept(nil, ofFunction: #function, atFile: #file, inLine: #line, withArgs: nil)
    }
    
    func dismissKeyboard() {
        callHandler.accept(nil, ofFunction: #function, atFile: #file, inLine: #line, withArgs: nil)
    }
    
    func removeSearchbarText() {
        callHandler.accept(nil, ofFunction: #function, atFile: #file, inLine: #line, withArgs: nil)
    }
    
    func updateSearchBarText(text: String) {
        callHandler.accept(nil, ofFunction: #function, atFile: #file, inLine: #line, withArgs: text)
    }
    
    func hideLoadingView() {
        callHandler.accept(nil, ofFunction: #function, atFile: #file, inLine: #line, withArgs: nil)
    }
    
    let callHandler: CallHandler
    
    init(testCase: XCTestCase) {
        callHandler = CallHandlerImpl(withTestCase: testCase)
    }
    
    func instanceType() -> MockSearchView {
        return self
    }
}

class MockCoreNetwork<Result: Codable>: CoreNetworkProtocol {
    var result: Result?
    var error: Error?
    
    func makeRequest<T: Codable>(request: RequestSpecs<T>,
                                 completion: @escaping (T?, Error?) -> Void) {
        if let result = self.result as? T {
            completion(result, error)
        } else {
            completion(nil, self.error)
            
        }
    }
}

extension XCTestCase {
    func wait(for seconds: Double) {
        let expectation = XCTestExpectation.init()
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds, execute: {
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: (seconds + 2))
    }
}

class MockSearchPresenter: SearchPresenter, Mock {
    let callHandler: CallHandler
    
    init(view: SearchView,interactor: SearchInteractor, testCase: XCTestCase) {
        callHandler = CallHandlerImpl(withTestCase: testCase)
        super.init(view: view, interactor: interactor, searchType: .querySearch)
    }
    
    func instanceType() -> MockSearchPresenter {
        return self
    }
}
