//
//  TestDocumentDetailsModule.swift
//  iOS TaskTests
//
//  Created by Mohamed Hassan Nawar on 27/06/2021.
//

import XCTest
import Foundation
import Mockit

class TestDocumentDetailsModule: XCTestCase {
    var presenter: MockDocumentDetailsPresenter!
    var view: MockDocumentDetailsView!
    var document = Document(title: "title", authorName: ["A"], isbn: ["122323"])
    
    override func setUpWithError() throws {
        view = MockDocumentDetailsView.init(testCase: self)
        presenter = MockDocumentDetailsPresenter(view: view, document: document, testCase: self)
    }
    
    func getDocument(title: String?, authorName: [String]?, isbn: [String]?) -> Document {
        return Document(title: title, authorName: authorName, isbn: isbn)
    }
    
    func testConfigurator() throws {
        guard let searchViewController = SearchViewController.loadFromNib() else { assert(false) }
        guard let documentDetailsviewController = DocumentDetailsViewController.loadFromNib() else { assert(false) }
        DocumentDetailsConfigurator().configureModule(viewController: documentDetailsviewController, selectedDocument: document, delegate: searchViewController)
        assert(documentDetailsviewController.presenter != nil)
    }
    
    func testNoDocumentTitle() throws {
        let documentContent = getDocument(title: nil, authorName: ["sd"], isbn: ["232"])
        let presenter = DocumentDetailsPresenter(view: view, document: document)
        presenter.showDocumentTitle(title: documentContent.title)
    }
    
    func testNoAuthorName() throws {
        let documentContent = getDocument(title: "A", authorName: nil, isbn: ["232"])
        let presenter = DocumentDetailsPresenter(view: view, document: document)
        presenter.showDocumentAuthors(authorsNames: documentContent.authorName)
    }
    
    func testNoDocumentIsbns() throws {
        let documentContent = getDocument(title: "Af", authorName: ["sd"], isbn: nil)
        let presenter = DocumentDetailsPresenter(view: view, document: document)
        presenter.showDocumentIsbns(isbns: documentContent.isbn)
    }
    
    func testDownloadImageIfNoIsbn() throws {
        let documentContent = getDocument(title: "Af", authorName: ["sd"], isbn: nil)
        let imageView = UIImageView()
        imageView.downloaded(from: HostService.getImageLink(isbn: documentContent.isbn?.first ?? ""))
    }
    
    func testViewController() throws {
        guard let viewController = DocumentDetailsViewController.loadFromNib() else { assert(false) }
        viewController.loadViewIfNeeded()
        let parent = UINavigationController.init(rootViewController: viewController)
        viewController.viewWillAppear(false)
        viewController.viewWillDisappear(false)
        assert(!parent.isNavigationBarHidden)
        viewController.presenter = presenter
        wait(for: 3)
    }
}

class MockDocumentDetailsView: DocumentDetailsView, Mock {
    func getDocumentIsbnsList(with isbns: [String]) {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: isbns.first)
    }
    
    func getDocumentAuthorsNames(with authorName: [String]) {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: authorName.first)
    }
    
    func getDocumentTitle(with title: String) {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: title)
    }
    let callHandler: CallHandler
    
    init(testCase: XCTestCase) {
        callHandler = CallHandlerImpl(withTestCase: testCase)
    }
    
    func instanceType() -> DocumentDetailsView {
        return self
    }
}

class MockDocumentDetailsPresenter: DocumentDetailsPresenter, Mock {
    let callHandler: CallHandler
    
    init(view: MockDocumentDetailsView, document: Document, testCase: XCTestCase) {
        callHandler = CallHandlerImpl(withTestCase: testCase)
        super.init(view: view, document: document)
    }
    
    func instanceType() -> DocumentDetailsPresenter {
        return self
    }
}
