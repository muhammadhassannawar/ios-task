# Overview

This repo includes a sample project that demonstrates the MVP architecture patterns on iOS.

* Xcode 12.5
* Swift 5

project contain a list of documents, this list populated by typing search query, which contain documents title and author name and when click in one of this documents get document details which contain title, author name, ISBN (International Standard Book Number) and corresponding cover image for each ISBN, if available, and if user click on title or author name that will present all related documents in document list screen.

<img src="https://files.fm/thumb_show.php?i=gjryrh7ws" width="250"> <img src="https://files.fm/thumb_show.php?i=xjc5kun9u" width="250">

# Pods
In this project includes some of useful pods for iOS, such as:
* Mockit: Used For Unit Test


# Included API
* Use URLSession For API requests

This project requires the following API:
* Search by query (http://openlibrary.org/search.json?q=)
* Search by document title  (http://openlibrary.org/search.json?title=)
* Search by Author name  ( http://openlibrary.org/search.json?author=)
* Note: An API call will not return more than 100 results and it contain pagination (&page=)
* Cover image by document ISBNs  (http://covers.openlibrary.org/b/isbn/<isbn-value>-<size>.jpg)
* Note:  <isbn-value> is document isbn <size> is the cover size specified as S, M or L
