//
//  CoverTableViewCell.swift
//  iOS Task
//
//  Created by Mohamed Hassan Nawar on 26/06/2021.
//

import UIKit

protocol CoverCellView {
    func configure(isbn: String)
}

class CoverTableViewCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak private var isbnImageView: UIImageView!
    @IBOutlet weak private var isbnLabel: UILabel!
    
    // MARK: - Lifecycle Methods
    override func prepareForReuse() {
        super.prepareForReuse()
        isbnImageView.image = nil
    }
}

// MARK: - View Protocol
extension CoverTableViewCell: CoverCellView {
    func configure(isbn: String) {
        isbnLabel.text = isbn
        isbnImageView.downloaded(from: HostService.getImageLink(isbn: isbn))
    }
}
