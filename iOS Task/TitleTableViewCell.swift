//
//  TitleTableViewCell.swift
//  iOS Task
//
//  Created by Mohamed Hassan Nawar on 26/06/2021.
//

import UIKit

protocol TitleCellView {
    func configure(title: String)
}

class TitleTableViewCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak private var titleLabel: UILabel!
}

// MARK: - View Protocol
extension TitleTableViewCell: TitleCellView {
    func configure(title: String) {
        titleLabel.text = title
    }
}
