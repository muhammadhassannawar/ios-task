//
//  SearchResult.swift
//  iOS Task
//
//  Created by Mohamed Hassan Nawar on 26/06/2021.
//

import Foundation

struct SearchResult: Codable {
    var start: Int
    var docs: [Document]
}

struct Document: Codable {
    let title: String?
    let authorName: [String]?
    let isbn: [String]?
    
    enum CodingKeys: String, CodingKey {
        case title
        case authorName = "author_name"
        case isbn
    }
}

/// Frame of search styles.
enum SearchType: String {
    case querySearch, documentTitleSearch, authorNameSearch
    var identifier: String {
        switch self {
        case .querySearch: return "q"
        case .documentTitleSearch: return "title"
        case .authorNameSearch: return "author"
        }
    }
}
