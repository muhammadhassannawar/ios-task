//
//  DocumentDetailsConfigurator.swift
//  iOS Task
//
//  Created by Mohamed Hassan Nawar on 26/06/2021.
//

import Foundation

class DocumentDetailsConfigurator {
    /// Configure the requirements and dependencies of this DocumentDetailsViewController
    /// - Parameter viewController: DocumentDetailsViewController
    func configureModule(viewController: DocumentDetailsViewController,
                         selectedDocument: Document,
                         delegate: SelectTitleOrAunthorDelegate?) {
        let presenter = DocumentDetailsPresenter(view: viewController, document: selectedDocument)
        viewController.presenter = presenter
        viewController.delegate = delegate
    }
}
