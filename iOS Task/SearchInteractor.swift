//
//  SearchInteractor.swift
//  iOS Task
//
//  Created by Mohamed Hassan Nawar on 25/06/2021.
//

import Foundation

protocol SearchInteractorProtocol {
    func loadDocuments(searchType: SearchType, page: Int, searchText: String, completionHundler: @escaping (SearchResult?, String?) -> Void)
}

class SearchInteractor {
    private let limit = 100
    private let coreNetwork: CoreNetworkProtocol
    
    init(coreNetwork: CoreNetworkProtocol) {
        self.coreNetwork = coreNetwork
    }
}
// MARK: - Interactor Protocol methods
extension SearchInteractor: SearchInteractorProtocol {
    /// load documents from API
    /// - Parameters:
    ///   - searchType: Get search type which may be query or document title or author name
    ///   - page: page number the request get only 100 element per page
    ///   - searchText: Name we need to search for
    ///   - completionHundler: Return search result or an error in case fail request
    func loadDocuments(searchType: SearchType, page: Int, searchText: String, completionHundler: @escaping (SearchResult?, String?) -> Void) {
        let parameter = [searchType.identifier: searchText, "page": page, "limit": limit] as [String: AnyObject]
        let request = RequestSpecs<SearchResult>(method: .GET, urlString: "search.json", parameters: parameter)
        coreNetwork.makeRequest(request: request, completion: { model, error in
            completionHundler(model, error?.localizedDescription)
        })
    }
}
