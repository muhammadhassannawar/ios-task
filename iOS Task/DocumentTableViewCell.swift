//
//  DocumentTableViewCell.swift
//  iOS Task
//
//  Created by Mohamed Hassan Nawar on 26/06/2021.
//

import UIKit

protocol DocumentsCellView {
    func configure(document: Document)
}

class DocumentTableViewCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak private var documentAuthorLabel: UILabel!
    @IBOutlet weak private var documentTitleLabel: UILabel!
}

// MARK: - View Protocol
extension DocumentTableViewCell: DocumentsCellView {
    func configure(document: Document) {
        documentAuthorLabel.text = document.authorName?.joined(separator: ", ")
        documentTitleLabel.text = document.title
    }
}
