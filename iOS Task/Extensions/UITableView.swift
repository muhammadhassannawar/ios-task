//
//  UITableView.swift
//  iOS Task
//
//  Created by Mohamed Hassan Nawar on 26/06/2021.
//

import UIKit

// MARK: Register and dequeue table view cell
extension UITableView {
    func register<T: UITableViewCell>(type: T.Type) {
        register(UINib.init(nibName: T.nibName, bundle: nil), forCellReuseIdentifier: T.identifier)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.identifier, for: indexPath) as? T else {
            fatalError("\(T.self) is expected to have reusable identifier: \(T.identifier)")
        }
        return cell
    }
}

// MARK: Get cell identifier and cell name
extension UITableViewCell {
    class var nibName: String {
        return "\(self)"
    }
    class var identifier: String {
        return "\(self)"
    }
}
