//
//  ViewController.swift
//  iOS Task
//
//  Created by Mohamed Hassan Nawar on 26/06/2021.
//

import UIKit

// MARK: Regiser view from nibName
extension UIViewController {
    class var nibName: String {
        return "\(self)"
    }
    
    static func loadFromNib() -> Self? {
        return Self(nibName: nibName, bundle: nil )
    }
}

// MARK: Show alert message
extension UIViewController {
    func showMessage(_ text: String) {
        let alert = UIAlertController.init(title: nil, message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: Customize navigation bar color
extension UIViewController {
    func setupNavigationBar() {
        self.navigationController?.navigationBar.tintColor = .black
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)]
    }
}
