//
//  UIImageView.swift
//  iOS Task
//
//  Created by Mohamed Hassan Nawar on 26/06/2021.
//

import UIKit

// MARK: Handel Download Image And Progress Indicator
extension UIImageView {
    /// Downloading image using URLSession
    /// - Parameters:
    ///   - link: The link of image as url
    ///   - mode: Content mode of image in image view
    func downloaded(from link: String, contentMode mode: ContentMode = .scaleToFill) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
    
    func downloaded(from url: URL, contentMode mode: ContentMode = .scaleToFill) {
        contentMode = mode
        self.configureLoadingIndicator()
        DispatchQueue.global().async {
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard
                    let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                    let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                    let data = data, error == nil,
                    let image = UIImage(data: data)
                else { return }
                DispatchQueue.main.async() { [weak self] in
                    self?.stopLoadingAnimation()
                    self?.image = image
                }
            }.resume()
        }
    }
    
    func stopLoading() {
        stopLoadingAnimation()
    }
    
    func stopLoadingAnimation() {
        guard let currentIndicator = subviews.first as? UIActivityIndicatorView else { return }
        currentIndicator.stopAnimating()
    }
    
    /// Adding activity indicator view in image view and start animating while download image
    fileprivate func configureLoadingIndicator() {
        if let currentIndicator = subviews.first as? UIActivityIndicatorView {
            currentIndicator.startAnimating()
            return
        }
        addIndicatorToTheCenter().startAnimating()
    }
    
    private func addIndicatorToTheCenter() -> UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
        self.addSubview(activityIndicator)
        activityIndicator.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        return activityIndicator
    }
    
}
