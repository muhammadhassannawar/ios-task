//
//  Array.swift
//  iOS Task
//
//  Created by Mohamed Hassan Nawar on 26/06/2021.
//

import Foundation

// MARK: Return Array of specific count of elements from another array of found
extension Array {
    func takeElements(elementCount: Int) -> Array {
        if (elementCount > count) {
            return Array(self[0..<count])
        }
        return Array(self[0..<elementCount])
    }
}
