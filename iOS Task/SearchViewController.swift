//
//  SearchViewController.swift
//  iOS Task
//
//  Created by Mohamed Hassan Nawar on 25/06/2021.
//

import UIKit

protocol SearchView: AnyObject {
    func showMessage(text: String)
    func showSearchResults(with results: [Document])
    func hideLoadingView()
    func showLoadingView()
    func dismissKeyboard()
    func removeSearchbarText()
    func updateSearchBarText(text: String)
}

class SearchViewController: UIViewController {
    // MARK: - Properties And Outlets
    var presenter: SearchPresenter?
    var documents: [Document] = []
    @IBOutlet weak private var loadingView: UIActivityIndicatorView!
    @IBOutlet weak private var searchTermsField: UISearchBar!
    @IBOutlet weak private var documentsTableView: UITableView!{
        didSet {
            setupTableView()
        }
    }
    
    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Documents"
        setupSearchBar()
        self.setupNavigationBar()
    }
}

// MARK: - UI Search Bar Setup And Delegate
extension SearchViewController: UISearchBarDelegate {
    func setupSearchBar() {
        searchTermsField.delegate = self
        searchTermsField.barTintColor = UIColor.clear
        searchTermsField.searchTextField.textColor = .black
        searchTermsField.backgroundColor = UIColor.clear
        searchTermsField.isTranslucent = true
        searchTermsField.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        searchTermsField.showsCancelButton = true
        let toolbar = UIToolbar()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                        target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: ""), style: .done,
                                         target: self, action: #selector(clickCancelButtonOnKeyboard))
        toolbar.setItems([flexSpace, doneButton], animated: true)
        toolbar.sizeToFit()
        searchTermsField.inputAccessoryView = toolbar
    }
    
    @objc func clickCancelButtonOnKeyboard() {
        presenter?.didClickKeyboardCancelButton()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else { return }
        presenter?.didPressSearch(searchText: text)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        presenter?.searchBarCancelButtonClicked()
    }
}

// MARK: - View Protocol
extension SearchViewController: SearchView {
    func updateSearchBarText(text: String) {
        self.searchTermsField.text = text
    }
    
    func dismissKeyboard() {
        self.searchTermsField.resignFirstResponder()
    }
    
    func removeSearchbarText() {
        self.searchTermsField.text = ""
    }
    
    func showMessage(text: String) {
        self.showMessage(text)
    }
    
    func hideLoadingView() {
        loadingView.stopAnimating()
    }
    
    func showLoadingView() {
        loadingView.startAnimating()
    }
    
    func showSearchResults(with results: [Document]) {
        documents = results
        self.documentsTableView.reloadData()
    }
}

// MARK: - UITableView Data Source
extension SearchViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return documents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return getConfiguredDocumentCell(tableView: tableView, for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (documents.count) - 1 {
            presenter?.didScrollToLast()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let document = documents[indexPath.row]
        guard let viewController = DocumentDetailsViewController.loadFromNib() else { return }
        DocumentDetailsConfigurator()
            .configureModule(viewController: viewController,
                             selectedDocument: document, delegate: self)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func getConfiguredDocumentCell(tableView: UITableView, for index: IndexPath) -> UITableViewCell {
        let document = documents[index.row]
        let cell: DocumentTableViewCell? = tableView.dequeueReusableCell(for: index)
        cell?.configure(document: document)
        return cell ?? UITableViewCell()
    }
    
    func setupTableView() {
        documentsTableView.delegate = self
        documentsTableView.dataSource = self
        documentsTableView.separatorStyle = .singleLine
        documentsTableView.separatorColor = .gray
        documentsTableView.keyboardDismissMode = .onDrag
        documentsTableView.tableFooterView = UIView()
        documentsTableView?.register(type: DocumentTableViewCell.self)
    }
}

// MARK: - Select Title Or Author From Document Details Protocol
extension SearchViewController: SelectTitleOrAunthorDelegate {
    func didSelectDocumentTitle(title: String) {
        presenter?.didChangeSearchType(searchType: .documentTitleSearch)
        presenter?.searchBySpecificText(text: title)
    }
    
    func didSelectDocumentAutherName(author: String) {
        presenter?.didChangeSearchType(searchType: .authorNameSearch)
        presenter?.searchBySpecificText(text: author)
    }
}
