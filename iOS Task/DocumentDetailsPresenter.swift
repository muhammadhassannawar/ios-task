//
//  DocumentDetailsPresenter.swift
//  iOS Task
//
//  Created by Mohamed Hassan Nawar on 26/06/2021.
//

import Foundation

protocol DocumentDetailsPresenterProtocol: AnyObject {
    func viewDidLoad()
}

class DocumentDetailsPresenter {
    // MARK: - Private Properties
    private let view: DocumentDetailsView
    private var document: Document
    private let isbnLimit = 5
    init(view: DocumentDetailsView, document: Document) {
        self.view = view
        self.document = document
    }
}

// MARK: - Presenter Protocol methods
extension DocumentDetailsPresenter: DocumentDetailsPresenterProtocol {
    func viewDidLoad() {
        showDocumentTitle(title: document.title)
        showDocumentAuthors(authorsNames: document.authorName)
        showDocumentIsbns(isbns: getFirstFiveIsbns(isbns: document.isbn))
    }
    
    func showDocumentTitle(title: String?) {
        guard let title = title else { return }
        view.getDocumentTitle(with: title)
    }
    
    func showDocumentAuthors(authorsNames: [String]?) {
        guard let authorName = authorsNames else { return }
        view.getDocumentAuthorsNames(with: authorName)
    }
    
    func showDocumentIsbns(isbns: [String]?) {
        guard let isbns = isbns  else {return }
        self.view.getDocumentIsbnsList(with: isbns)
    }
    
    func getFirstFiveIsbns(isbns: [String]?) -> [String]? {
        return isbns?.takeElements(elementCount: isbnLimit)
    }
}
