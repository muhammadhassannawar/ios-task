//
//  SceneDelegate.swift
//  iOS Task
//
//  Created by Mohamed Hassan Nawar on 25/06/2021.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    
    func scene(_ scene: UIScene,
               willConnectTo session: UISceneSession,
               options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        let window = UIWindow(windowScene: windowScene)
        let viewController = SearchViewController.loadFromNib()
        if let viewController = viewController {
            SearchConfigurator().configure(viewController: viewController)
        }
        window.rootViewController = UINavigationController(rootViewController: viewController ?? UIViewController())
        self.window = window
        window.makeKeyAndVisible()
    }
}
