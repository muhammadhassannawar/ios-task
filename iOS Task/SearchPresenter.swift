//
//  SearchPresenter.swift
//  iOS Task
//
//  Created by Mohamed Hassan Nawar on 25/06/2021.
//

import Foundation

protocol SearchPresenterProtocol: AnyObject {
    func didPressSearch(searchText: String)
    func didClickKeyboardCancelButton()
    func searchBarCancelButtonClicked()
    func didScrollToLast()
    func didChangeSearchType(searchType: SearchType)
    func searchBySpecificText(text: String)
}

class SearchPresenter {
    // MARK: - Private Properties
    private let view: SearchView
    private let interactor: SearchInteractor?
    private var isLoading = false
    private var areThereMoreDocuments = true
    private var currenyPage = 0
    private var searchText = ""
    private var documentModels: [Document] = []
    private var searchType: SearchType
    
    init(view: SearchView, interactor: SearchInteractor?, searchType: SearchType) {
        self.view = view
        self.interactor = interactor
        self.searchType = searchType
    }
    
    // MARK: - Private methods
    private func loadDocuments(searchType: SearchType, searchText: String, page: Int, completion: (()->Void)? = nil) {
        isLoading = true
        interactor?.loadDocuments(searchType: searchType, page: page, searchText: searchText) { [weak self] (result, error) in
            completion?()
            self?.isLoading = false
            if let _ = error {
                self?.view.showMessage(text: "Couldn't load page \(page)")
            } else if let result = result {
                self?.didLoadDocuments(models: result.docs, page: page)
            }
        }
    }
    
    /// Take array of documents to organize documents and check if there is no more
    ///  documents to stop loading more and update view with results
    /// - Parameters:
    ///   - models: List of documents
    ///   - page: page number
    private func didLoadDocuments(models: [Document], page: Int) {
        if page == 1 {
            self.documentModels = models
        } else {
            if models.isEmpty {
                areThereMoreDocuments = false
            }else{
                self.documentModels.append(contentsOf: models)
                areThereMoreDocuments = true
            }
        }
        currenyPage = max(page, currenyPage)
        view.showSearchResults(with: documentModels)
    }
}

// MARK: - Presenter Protocol methods
extension SearchPresenter: SearchPresenterProtocol {
    func searchBySpecificText(text: String) {
        self.view.updateSearchBarText(text: text)
        self.searchText = text
        guard !isLoading else { return }
        view.showLoadingView()
        loadDocuments(searchType: searchType, searchText: searchText, page: 1) {
            self.view.hideLoadingView()
        }
    }
    
    func didChangeSearchType(searchType: SearchType) {
        self.searchType = searchType
    }
    
    func didClickKeyboardCancelButton() {
        cancelSearch()
    }
    
    func didPressSearch(searchText: String) {
        self.searchText = searchText
        self.view.dismissKeyboard()
        guard !isLoading else { return }
        view.showLoadingView()
        loadDocuments(searchType: searchType, searchText: searchText, page: 1) {
            self.view.hideLoadingView()
        }
    }
    
    func searchBarCancelButtonClicked() {
        cancelSearch()
    }
    
    func didScrollToLast() {
        // Check if no loading document request fetch or if we not reatch last elelment in search
        guard !isLoading && areThereMoreDocuments else { return }
        view.showLoadingView()
        loadDocuments(searchType: searchType, searchText: searchText, page: currenyPage + 1) {
            self.view.hideLoadingView()
        }
    }
    
    func cancelSearch() {
        self.view.dismissKeyboard()
        self.view.removeSearchbarText()
    }
}
