//
//  DocumentDetailsViewController.swift
//  iOS Task
//
//  Created by Mohamed Hassan Nawar on 26/06/2021.
//

import UIKit

protocol DocumentDetailsView: AnyObject {
    func getDocumentIsbnsList(with isbns: [String])
    func getDocumentAuthorsNames(with authorName: [String])
    func getDocumentTitle(with title: String)
}

protocol SelectTitleOrAunthorDelegate: AnyObject {
    func didSelectDocumentTitle(title: String)
    func didSelectDocumentAutherName(author: String)
}

class DocumentDetailsViewController: UIViewController {
    // MARK: - Properties And Outlets
    var presenter: DocumentDetailsPresenter?
    var isbns: [String] = []
    var authors: [String] = []
    var documentTitle: String = ""
    weak var delegate: SelectTitleOrAunthorDelegate?
    @IBOutlet weak private var loadingView: UIActivityIndicatorView!
    @IBOutlet weak private var documentDetailsTableView: UITableView! {
        didSet {
            setupTableView()
        }
    }
    
    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Document Details"
        self.setupNavigationBar()
        presenter?.viewDidLoad()
    }
}

// MARK: - UITableView Data Source
extension DocumentDetailsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return authors.count
        case 2:
            return isbns.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return getConfiguredDocumentCell(tableView: tableView, for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            self.delegate?.didSelectDocumentTitle(title: documentTitle)
            self.navigationController?.popViewController(animated: true)
        case 1:
            self.delegate?.didSelectDocumentAutherName(author: authors[indexPath.row])
            self.navigationController?.popViewController(animated: true)
        default: break
        }
    }
    
    func getConfiguredDocumentCell(tableView: UITableView, for index: IndexPath) -> UITableViewCell {
        switch index.section {
        case 0:
            let cell: TitleTableViewCell? = tableView.dequeueReusableCell(for: index)
            cell?.configure(title: documentTitle)
            return cell ?? UITableViewCell()
        case 1:
            let cell: TitleTableViewCell? = tableView.dequeueReusableCell(for: index)
            cell?.configure(title: authors[index.row])
            return cell ?? UITableViewCell()
        case 2:
            let cell: CoverTableViewCell? = tableView.dequeueReusableCell(for: index)
            cell?.configure(isbn: isbns[index.row])
            return cell ?? UITableViewCell()
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Title"
        case 1:
            return "Authors names"
        case 2:
            return "ISBNs"
        default:
            return ""
        }
    }
    
    private func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        return headerView
    }
    
    func setupTableView() {
        documentDetailsTableView.delegate = self
        documentDetailsTableView.dataSource = self
        documentDetailsTableView.separatorStyle = .none
        documentDetailsTableView.tableFooterView = UIView()
        documentDetailsTableView?.register(type: TitleTableViewCell.self)
        documentDetailsTableView?.register(type: CoverTableViewCell.self)
    }
}

// MARK: - View Protocol
extension DocumentDetailsViewController: DocumentDetailsView {
    func getDocumentTitle(with title: String) {
        self.documentTitle = title
        self.documentDetailsTableView.reloadSections([0], with: .automatic)
    }
    
    func getDocumentAuthorsNames(with authorName: [String]) {
        self.authors = authorName
        self.documentDetailsTableView.reloadSections([1], with: .automatic)
    }
    
    func getDocumentIsbnsList(with isbns: [String]) {
        self.isbns = isbns
        self.documentDetailsTableView.reloadSections([2], with: .automatic)
    }
}
