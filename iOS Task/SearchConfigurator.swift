//
//  SearchConfigurator.swift
//  iOS Task
//
//  Created by Mohamed Hassan Nawar on 26/06/2021.
//

import Foundation

class SearchConfigurator {
    /// Configure the requirements and dependencies of this SearchViewController
    /// - Parameter viewController: SearchViewController
    func configure(viewController: SearchViewController) {
        let coreNetwork = CoreNetwork()
        let interactor = SearchInteractor(coreNetwork: coreNetwork)
        let presenter = SearchPresenter(view: viewController, interactor: interactor, searchType: .querySearch)
        viewController.presenter = presenter
    }
}
