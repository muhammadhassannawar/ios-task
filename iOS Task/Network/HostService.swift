//
//  HostService.swift
//  iOS Task
//
//  Created by Mohamed Hassan Nawar on 25/06/2021.
//

import Foundation

struct HostService {
    // MARK: - static Properties And Methods
    static var headers: [String: String] { [:] }
    
    static func getBaseURL() -> String {
        return "http://openlibrary.org/"
    }
    
    static func getImageLink(isbn: String) -> String {
        return "http://covers.openlibrary.org/b/isbn/\(isbn)-L.jpg"
    }
}
