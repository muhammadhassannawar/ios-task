//
//  NetworkInterface.swift
//  iOS Task
//
//  Created by Mohamed Hassan Nawar on 25/06/2021.
//

import Foundation

enum RequestMethod: String {
    case GET, POST, PUT, DELETE
}

/// Request specification to collect all related data to perform a request which contain method type, url and parameters
struct RequestSpecs<ResponseType: Decodable> {
    let method: RequestMethod
    let urlString: String
    let parameters: [String: AnyObject]?
    
    init(method: RequestMethod, urlString: String,
         parameters: [String: AnyObject]?) {
        self.method = method
        self.urlString = urlString
        self.parameters = parameters
    }
}

protocol NetworkingProtocol {
    func request<T: Decodable>(_ specs: RequestSpecs<T>,
                               completionBlock: @escaping (T?, Error?) -> Void)
}
