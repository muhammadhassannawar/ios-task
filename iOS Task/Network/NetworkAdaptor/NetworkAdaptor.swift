//
//  NetworkAdaptor.swift
//  iOS Task
//
//  Created by Mohamed Hassan Nawar on 25/06/2021.
//

import Foundation
import Combine

class NetworkAdapter {
    // MARK: - private Properties
    private let requestBaseURL: String
    private let requestHeaders: [String: String]
    private var cancellable: Set<AnyCancellable> = []
    
    init(baseURL: String, headers: [String: String]) {
        self.requestBaseURL = baseURL
        self.requestHeaders = headers
    }
    
    /// Create get request
    /// - Parameter specs: Get request specifications which contain all related data to request
    /// - Returns: Query url from attributes in request specifications
    /// It works in case 'GET' requests only because it used to get query url parameters from parameter added to request specifications
    private func createGetRequest<T>(specs: RequestSpecs<T>) -> URLRequest {
        var components = URLComponents(string: self.requestBaseURL + specs.urlString)!
        components.queryItems = (specs.parameters?.map({ URLQueryItem(name: $0, value:  String(describing: $1))}) ?? [])
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        var request = URLRequest(url:  components.url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60)
        print("url::\(components.url!)")
        request.httpMethod = getRequestMethod(method: specs.method)
        for header in requestHeaders { request.addValue(header.value, forHTTPHeaderField: header.key) }
        return request
    }
    
    /// API sevice request
    /// - Parameters:
    ///   - specs: Get request specifications which contain all related data to request
    ///   - completionBlock: Which contain a value or error message
    func request<T>(_ specs: RequestSpecs<T>, completionBlock: @escaping (T?, Error?) -> Void) where T : Decodable {
        let request = createGetRequest(specs: specs)
        URLSession.shared.dataTaskPublisher(for: request)
            .receive(on: DispatchQueue.main)
            .tryMap() { element -> Data in
                guard let httpResponse = element.response as? HTTPURLResponse,
                      httpResponse.statusCode == 200 else {
                    throw URLError(.badServerResponse)
                }
                return element.data
            }.decode(type: T.self, decoder: JSONDecoder())
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished: break
                case .failure(let error): completionBlock(nil, error)
                }
            }, receiveValue: { completionBlock($0, nil) })
            .store(in: &cancellable)
    }
    
    private func getRequestMethod(method: RequestMethod)-> String {
        switch method {
        case .GET: return "GET"
        case .POST: return "POST"
        case .DELETE: return "DELETE"
        case .PUT: return "PUT"
        }
    }
}

// MARK: - Core Network Use Any Network Adapter That Conform Networking Protocol
extension NetworkAdapter: NetworkingProtocol {}
