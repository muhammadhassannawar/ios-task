//
//  CoreNetwork.swift
//  iOS Task
//
//  Created by Mohamed Hassan Nawar on 25/06/2021.
//

import Foundation

protocol CoreNetworkProtocol {
    func makeRequest<T: Codable>(request: RequestSpecs<T>,
                                 completion: @escaping (T?, Error?) -> Void)
}

class CoreNetwork {
    // MARK: - Properties
    static var sharedInstance: CoreNetworkProtocol = CoreNetwork()
    fileprivate lazy var networkCommunication: NetworkingProtocol = {
        NetworkAdapter(baseURL: HostService.getBaseURL(), headers: HostService.headers)
    }()
}

// MARK: - Core Network Protocol methods
extension CoreNetwork: CoreNetworkProtocol {
    func makeRequest<T: Codable>(request: RequestSpecs<T>,
                                 completion: @escaping (T?, Error?) -> Void) {
        networkCommunication.request(request, completionBlock: completion)
    }
}
